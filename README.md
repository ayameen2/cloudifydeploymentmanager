**Exercise: Cloudify & Python**

## Objective
Write a Python script that reads a description of Cloudify instructions from a YAML file, and act upon them.

## Input
The Python program should receive the following parameters:

1. IP address of a Cloudify Manager
2. User to authenticate with
3. Password to authenticate with
4. Tenant ID to access
5. Path to inputs file

## Input Example

```
<blueprint_id>:
    path: <path_to_blueprint>
    deployments:
        <deployment_id>:
            inputs:
                - <path_to_inputs_file>
                - <path_to_another_inputs_file>
                - ...
        <deployment_id>:
            inputs:
                - <path_to_inputs_file>
                - <path_to_another_inputs_file>
                - ...
<another_blueprint_id>:
    path: <path_to_another_blueprint>
    deployments:
        <deployment_id>:
            inputs:
                - <path_to_inputs_file>
                - <path_to_another_inputs_file>
                - ...
        <deployment_id>:
            inputs:
                - <path_to_inputs_file>
                - <path_to_another_inputs_file>
                - ...
```

## Run cloudify deployment manager

`
python deployment_manager.py -username='' -password='' -tenant_id='' -host='' -input_file=''
`

