#!/usr/bin/python

import yaml
import sys
import argparse

import cloudify_rest_client


def parse_yml_file(yml_file_path):
    """

    :param yml_file_path: yml file path to be parsed as dict
    :return: dict representation for the provided yml file
    """
    stream = file(yml_file_path, 'r')
    result = yaml.load(stream)
    stream.close()
    return result


def upload_blueprint(cfy_client, blueprint_name, blueprint_file):
    """
    Upload the provided blueprint to Cloudify manager
    :param cfy_client:
    :param blueprint_name: blueprint ID in Cloudify manager
    :param blueprint_file: blueprint yml file path
    :return:
    """
    try:
        cfy_client.blueprints.upload(blueprint_file, blueprint_id=blueprint_name)
    except Exception as error:
        print 'Failed to upload blueprint {}'.format(blueprint_name)
        raise error


def create_blueprint_deployments(cfy_client, blueprint_name, blueprint):
    """
    Create blueprint's deployments

    :param cfy_client: Cloudify rest client instance
    :param blueprint_name: bluerint ID that already uploaded to the Cloudify manager
    :param blueprint: blueprint dict, contains the a deployments with their input files
    :return:
    """
    blueprint_deployments = blueprint.get('deployments')
    deployments_names = [deployment_name for deployment_name in blueprint_deployments]
    for deployment_name in deployments_names:
        deployment = blueprint_deployments.get(deployment_name)

        deployment_inputs = deployment.get('inputs')
        all_inputs = dict()
        for input_path in deployment_inputs:
            try:
                all_inputs.update(parse_yml_file(input_path))
            except IOError as error:
                print 'Deployment {} inputs file {} not found'.format(deployment_name, input_path)
                raise error

        cfy_client.deployments.create(blueprint_name, deployment_name, all_inputs)


def main(cfy_client, blueprint_path):
    """
    This is the entry point
    :param cfy_client: Instance of the cloudify rest client
    :param blueprint_path: Input file path, the blueprint of the required deployments is resides in
    :return:
    """
    try:
        # Get the blueprint input file as dict
        deployment_manager_blueprint = parse_yml_file(blueprint_path)
    except IOError as error:
        print 'Deployment manager blueprint not found'
        raise error

    # Iterate over blueprints list to upload it and create the related deployments
    blueprints_name = [blueprint_name for blueprint_name in deployment_manager_blueprint]
    for blueprint_name in blueprints_name:
        blueprint = deployment_manager_blueprint.get(blueprint_name)

        # Upload the blueprint to the Cloudify manager
        upload_blueprint(cfy_client, blueprint_name, blueprint.get('path'))

        # Create the blueprint's deployments on the Cloudify manager
        create_blueprint_deployments(cfy_client, blueprint_name, blueprint)


if __name__ == '__main__':

    # This script will parse the input_file and upload the provided blueprints and
    # create their deployments:
    #
    # It will receive 5 parameter as follow,
    # host: Cloudify manager IP
    # username: Cloudify manager username
    # password: Cloudify manager password
    # tenant_id: Cloudify manager tenant ID
    # input_file: Blueprint file for the required deployments

    parser = argparse.ArgumentParser()
    parser.add_argument('host', help='Cloudify manager IP')
    parser.add_argument('username', help='Cloudify manager username')
    parser.add_argument('password', help='Cloudify manager password')
    parser.add_argument('tenant_id', help='Cloudify manager tenant ID')
    parser.add_argument('input_file', help='Blueprint file for the required deployments')

    args = parser.parse_args()

    client = cloudify_rest_client.CloudifyClient(host=args.host,
                                                 username=args.username,
                                                 password=args.password,
                                                 tenant=args.tenant_id)

    main(client, args.input_file)